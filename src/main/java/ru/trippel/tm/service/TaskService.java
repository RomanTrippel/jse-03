package ru.trippel.tm.service;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.repository.TaskRepository;
import ru.trippel.tm.util.UuidUtil;
import ru.trippel.tm.view.KeyboardView;
import ru.trippel.tm.view.ProjectView;
import ru.trippel.tm.view.TaskView;

import java.io.IOException;
import java.util.UUID;

public class TaskService {

    private KeyboardView readerKeyboard = new KeyboardView();

    private TaskRepository taskRepository = new TaskRepository();

    public void findAll() {
        TaskView.printList(taskRepository.findAll());
    }

    public void persist() throws Exception {
        Task task = new Task();
        String taskName = TaskView.addTask();
        task.setTaskId(UuidUtil.get());
        task.setName(taskName);
        taskRepository.persist(task);
    }

    public void remove() throws IOException {
        Task task;
        task = TaskView.removeTask(taskRepository.findAll());
        taskRepository.remove(task);
    }

    public void merge() throws IOException {
        Task task;
        task = TaskView.merge(taskRepository.findAll());
        taskRepository.merge(task);
    }

}
