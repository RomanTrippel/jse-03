package ru.trippel.tm.service;

import ru.trippel.tm.enumeration.TerminalCommand;
import ru.trippel.tm.view.HelpView;
import ru.trippel.tm.view.ProjectView;
import ru.trippel.tm.view.TerminalCommandView;

import java.io.IOException;

public class TerminalCommandService {

    ProjectService projectService = new ProjectService();

    TaskService taskService = new TaskService();

    public void check(String s) throws Exception {
        TerminalCommand command = null;
        try {
            command = TerminalCommand.valueOf(s);
        } catch (Exception e) {
            TerminalCommandView.printError();
            return;
        }
        switch (command) {
            case EXIT:
                System.exit(0);
                return;
            case HELP:
                HelpView.printCommandList();
                break;
            case PROJECT_CREATE:
                projectService.persist();
                break;
            case PROJECT_VIEW:
                projectService.findAll();
                break;
            case PROJECT_EDIT:
                projectService.merge();
                break;
            case PROJECT_REMOVE:
                projectService.remove();
                break;
            case PROJECT_ATTACHTASK:
                projectService.attachTask();
                break;
            case PROJECT_VIEWTASK:
                projectService.viewProjectTask();
                break;
            case TASK_CREATE:
                taskService.persist();
                break;
            case TASK_VIEW:
                taskService.findAll();
                break;
            case TASK_EDIT:
                taskService.merge();
                break;
            case TASK_REMOVE:
                taskService.remove();
                break;
        }
    }

}
