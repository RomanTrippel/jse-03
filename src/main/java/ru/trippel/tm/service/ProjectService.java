package ru.trippel.tm.service;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.repository.ProjectRepository;
import ru.trippel.tm.repository.TaskRepository;
import ru.trippel.tm.util.UuidUtil;
import ru.trippel.tm.view.KeyboardView;
import ru.trippel.tm.view.ProjectView;
import ru.trippel.tm.view.TaskView;
import java.io.IOException;
import java.util.ArrayList;

public class ProjectService {

    private KeyboardView readerKeyboard = new KeyboardView();

    private ProjectRepository projectRepository = new ProjectRepository();

    private TaskRepository taskRepository = new TaskRepository();

    public void findAll() {
        ProjectView.printList(projectRepository.findAll());
    }

    public void persist() throws Exception {
        Project project = new Project();
        String projectName = ProjectView.addProject();
        project.setProjectId(UuidUtil.get());
        project.setName(projectName);
        projectRepository.persist(project);
    }

    public void remove() throws IOException {
        Project project;
        String projectId;
        ArrayList<Task> taskListTemp = taskRepository.findAll();
        project = ProjectView.removeProject(projectRepository.findAll());
        projectId = project.getProjectId();
        projectRepository.remove(project);
        for (int i = 0; i < taskListTemp.size() ; i++) {
            if (projectId.equals(taskListTemp.get(i).getProjectId())){
                taskRepository.remove(taskListTemp.get(i));
            }
        }
    }

    public void merge() throws IOException {
        Project project;
        project = ProjectView.merge(projectRepository.findAll());
        projectRepository.merge(project);
    }

    public void attachTask() throws IOException {
        Task task;
        String projectId;
        projectId = ProjectView.attachTask(projectRepository.findAll());
        task = TaskView.attachTask(projectId, taskRepository.findAll());
        task.setProjectId(projectId);
        taskRepository.merge(task);
    }

    public void viewProjectTask() throws IOException {
        String projectId;
        projectId = ProjectView.viewTask(projectRepository.findAll());
        ArrayList<Task> taskListTemp = taskRepository.findAll();
        for (int i = 0; i < taskListTemp.size() ; i++) {
            if (projectId.equals(taskListTemp.get(i).getProjectId())){
                ProjectView.printTask(taskListTemp.get(i).getName());
            }
        }
    }

}
