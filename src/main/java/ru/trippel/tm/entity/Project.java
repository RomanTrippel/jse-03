package ru.trippel.tm.entity;

import java.util.Date;
import java.util.Objects;

public class Project {

    private String projectId = "";

    private String name = "";

    private String description = "";

    private Date dateStart = new Date();

    private Date dateFinish = new Date(2100);

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Objects.equals(projectId, project.projectId) &&
                Objects.equals(name, project.name) &&
                Objects.equals(description, project.description) &&
                Objects.equals(dateStart, project.dateStart) &&
                Objects.equals(dateFinish, project.dateFinish);
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectId, name, description, dateStart, dateFinish);
    }

}