package ru.trippel.tm.util;

import java.util.UUID;

public class UuidUtil {

    public static String get() {
        return UUID.randomUUID().toString().replace("-", "");
    }

}