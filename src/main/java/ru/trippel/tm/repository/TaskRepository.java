package ru.trippel.tm.repository;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;

import java.util.*;

public class TaskRepository extends AbstractRepository<Task> {

    @Override
    public List<Task> findAll() {
        return (List<Task>) map.values();
    }

    @Override
    public Task findOne(String id) {
        return map.get(id);
    }

    @Override
    public Task persist(Task task) {
        String taskId = task.getProjectId();
        if (map.containsKey(taskId)) return null;
        return map.put(taskId,task);
    }

    @Override
    public Task merge(Task task) {
        String taskId = task.getProjectId();
        return map.merge(taskId, task, (oldProject, newProject) -> newProject);
    }

    @Override
    public Task remove(String id) {
        return map.remove(id);
    }

    @Override
    public void removeAll() {
        map.clear();
    }

}
