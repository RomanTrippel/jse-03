package ru.trippel.tm;

import ru.trippel.tm.service.TerminalCommandService;
import ru.trippel.tm.view.KeyboardView;
import ru.trippel.tm.view.GreetingView;


public class Application {

    public static void main(String[] args) throws Exception {
        GreetingView greetingView = new GreetingView();
        KeyboardView readerKeyboard = new KeyboardView();
        TerminalCommandService terminalCommandService = new TerminalCommandService();
        greetingView.print();
        while (true) {
            terminalCommandService.check(readerKeyboard.read());
        }
    }

}