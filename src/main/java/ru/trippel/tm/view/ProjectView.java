package ru.trippel.tm.view;

import ru.trippel.tm.entity.Project;
import java.io.IOException;
import java.util.ArrayList;

public class ProjectView {

    public static void printList(ArrayList<Project> projectList) {
        Project project;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            project = projectList.get(i);
            System.out.println(i+1 + ". " + project.getProjectId() + " - " + project.getName());
        }
    }

    public static String addProject() throws IOException {
        System.out.println("Enter project name.");
        return KeyboardView.read();
    }

    public static Project removeProject(ArrayList<Project> projectList) throws IOException {
        printList(projectList);
        int projectNumber;
        System.out.println("Enter a project number.");
        projectNumber = Integer.parseInt(KeyboardView.read());
        return projectList.get(projectNumber - 1);
    }

    public static Project merge(ArrayList<Project> projectList) throws IOException {
        printList(projectList);
        int projectNumber;
        System.out.println("Enter a project number to Edit.");
        projectNumber = Integer.parseInt(KeyboardView.read());
        System.out.println("Enter new Name:");
        projectList.get(projectNumber - 1).setName(KeyboardView.read());
        return projectList.get(projectNumber - 1);
    }

    public static String attachTask(ArrayList<Project> projectList) throws IOException {
        printList(projectList);
        int projectNumber;
        System.out.println("Enter a project number to attach a Task.");
        projectNumber = Integer.parseInt(KeyboardView.read());
        return projectList.get(projectNumber - 1).getProjectId();
    }

    public static String viewTask(ArrayList<Project> projectList) throws IOException {
        printList(projectList);
        int projectNumber;
        System.out.println("Enter a project number to show all attached tasks.");
        projectNumber = Integer.parseInt(KeyboardView.read());
        System.out.println("List of tasks attached to the project:");
        return projectList.get(projectNumber - 1).getProjectId();
    }

    public static void printTask(String name) {
        System.out.println(name);
    }

}
