package ru.trippel.tm.view;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import java.io.IOException;
import java.util.ArrayList;

public class TaskView {

    public static void printList(ArrayList<Task> taskList) {
        Task task;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            task = taskList.get(i);
            System.out.println(i+1 + ". " + task.getTaskId() + " - " + task.getName());
        }
    }

    public static String addTask() throws IOException {
        System.out.println("Enter task name.");
        return KeyboardView.read();
    }

    public static Task removeTask(ArrayList<Task> taskList) throws IOException {
        printList(taskList);
        int taskNumber;
        System.out.println("Enter a task number.");
        taskNumber = Integer.parseInt(KeyboardView.read());
        return taskList.get(taskNumber - 1);
    }

    public static Task merge(ArrayList<Task> taskList) throws IOException {
        printList(taskList);
        int taskNumber;
        System.out.println("Enter a task number to Edit.");
        taskNumber = Integer.parseInt(KeyboardView.read());
        System.out.println("Enter new Name");
        taskList.get(taskNumber - 1).setName(KeyboardView.read());
        return taskList.get(taskNumber - 1);
    }

    public static Task attachTask(String projectId, ArrayList<Task> taskList) throws IOException {
        printList(taskList);
        int taskNumber;
        System.out.println("Enter a task number to Attach");
        taskNumber = Integer.parseInt(KeyboardView.read());
        taskList.get(taskNumber - 1).setProjectId(projectId);
        return taskList.get(taskNumber - 1);
    }

}