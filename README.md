# [Project Manager](https://gitlab.com/RomanTrippel/jse-04-1)

## Software:
```
* JDK 8
* Java 1.8
* Maven 4.0
* IntelliJ IDEA
* Windows 10
```

## Commands to build:
```bash
mvn clean
```
```bash
mvn install
```

## Command to run:
```cmd
java -jar target/project-manager-4.0.jar
```
### Developer:
```
Roman Trippel

rtrippel84@gmail.com
```